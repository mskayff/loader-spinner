/**
 * Spin Loader
 *
 * @author Mark Skayff
 */
var SpinLoader = {
	_options : null,
	_overlay : null,

	/**
	 * Setup your library path.
	 * this will depend on where your assets folder is located
	 */
	library_path : "js/spin.loader.js",	

	init : function(options){
		// ------------------------------------------------------
		// SpinLoader Options
		// ------------------------------------------------------
		// Merge passed options with the _options 
		// property object
		// ------------------------------------------------------
		this._options = $.extend({
			style : 
				options.style? options.style : "info",
			show  : 
				options.show ? options.show : false,
			overlay_style : 
				options.overlay_style? options.overlay_style : 'transparent', // Options are none, transparent, opaque			message : ""
		},options);
		// ----------------------------------------------------------------------------------------
		// Create the overlay element
		// ----------------------------------------------------------------------------------------
		this._overlay = document.createElement('div');
		// ----------------------------------------------------------------------------------------
		// Add a class to the overlay
		// ----------------------------------------------------------------------------------------
		$(this._overlay).addClass('spin-loader-overlay');
		// ----------------------------------------------------------------------------------------
		// Append the overlay to the document body
		// ----------------------------------------------------------------------------------------
		$('body').append(this._overlay);
		// --------------------------------------------------------
		// Show the spinner if want to show it straight away
		// --------------------------------------------------------
		if(this._options.show) this.show(options.message);

		return this;
	},

	show : function(msg){
		// Show the overlay
		this.showOverlay(true);		
		// Create a new spinnerBox element
		var spinnerBox = document.createElement('div');
		//spinnerBox.id = "spinner-box-wrapper";
		// Add the general class for spinner
		$(spinnerBox).addClass('spinner-box');

		// A more specific spinner CSS class
		var spClass = "";
		// Determine a more specific CSS class
		// depending on defined style.
		switch(this._options.style){
			case 'success' : spClass = 'spinner-style-1'; break;
			case 'danger'  : spClass = 'spinner-red-1'; break;
			case 'info'    : spClass = 'spinner-light-blue-1';break;
			default : spClass = 'spinner-style-1';
		}
		// ------------------------------------------------------------------------
		// Assign the new CSS class.
		// ------------------------------------------------------------------------
		$(spinnerBox).addClass(spClass);
		// ------------------------------------------------------------------------
		// Add html content to the spinner
		// ------------------------------------------------------------------------
		$(spinnerBox).html("<div id='spinner-msg'>" + msg + "</div><div><img src='" + this.library_path + "/spinner-white-1.png' alt='processing...' class='sp1'></div>");
		$('body').append(spinnerBox);
		$(spinnerBox).fadeIn(200);

		// ------------------------------------------------------------------------
		// Position the spinner on the screen
		// ------------------------------------------------------------------------		
		
		// ----------------------------------------------------------------------------------------
		// Calculate window width and height
		// ----------------------------------------------------------------------------------------
		var wWitdh = $(window).outerWidth();
		var wHeight = $(window).outerHeight();	
		// SEt the spinner box width
		var spinnerBoxWidth = $(spinnerBox).width();
		// Set the spinner's top and left positions
		$(spinnerBox).css({
				'top' : (wHeight/2) - 100,
				'left' :  (wWitdh/2) - (spinnerBoxWidth/2)});

		return this;
	},

	showOverlay : function (active){
		if(active){ 
			// Make the overlay active - display it
			var opacity;
			// ----------------------------------------------------------------------------------------
			// Determine the overlay's opacity
			// ----------------------------------------------------------------------------------------
			switch(this._options.overlay_style){
				case 'transparent' : 
					opacity = 0.5;
					break;
				case 'opaque':
					opacity : 1;
					break;
				default : opacity = 0;
			}
			// ----------------------------------------------------------------------------------------
			// Set overlay background color and opacity
			// ----------------------------------------------------------------------------------------
			$('.spin-loader-overlay').css({
				'backgroundColor' : '#555',
				'opacity' : opacity
			});
			// Activate the overlay by assigning a class to the body
			$('body').addClass('overlay-on');
		}
		else{
			// De-activate the overlay
			$('body').removeClass('overlay-on');
		}
	},

	/**
	 * Stop the spinner
	 */
	stop : function(){
		this.showOverlay(false);
		$(".spinner-box").remove();
	},


	/**
	 * Modify the spinner message dynamically
	 */
	sendMessage : function(message){
		if(document.getElementById('spinner-msg')){
			$('#spinner-msg').html(message);	
		}
		return this;
	}
};